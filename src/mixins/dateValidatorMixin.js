export default {
  data () {
    return {
      data: {
        initialDate: '',
        finalDate: ''
      }
    }
  },
  methods: {
    validateDate() {
      if (this.data.initialDate !== '' & this.data.finalDate !== '') {
        if (this.data.initialDate > this.data.finalDate) {
          this.error = 'Periodo inválido. Data inicial deve ser menor que a final.'
          this.data.finalDate = ''
          this.$refs.finalDate.focus();
        }else {
          this.error = ''
        }
      }
    }
  }
}